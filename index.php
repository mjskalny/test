<?php

class Klasa{

	public $result = 0;

	public function lorem()
	{
		echo "Hello World!";
	}

	public function ipsum()
	{
		$this->$result = 3;
	}

	public function zwroc()
	{
		return $this->$result;
	}
}

$pdo = new Klasa;

$pdo->lorem();

$pdo->ipsum();

$pdo->zwroc();

?>